function pi () {
  var π= 0;
  var num= 4;
  var den= 1;
  var plus= true;

  while (den < 1e7) {
    if (plus) {
      π+= num/den;
      plus= false;
    }
    else {
      π-= num/den;
      plus= true;
    }
    den+= 2;
  }
  return π;
}

var t= Date.now();
var r= pi();
t= Date.now()- t;
console.log([r, t+ " ms"]);

/*
pi@opi-buster:~/quickjs$ nodejs george/pi.js 
[ 3.1415924535897797, '254ms' ]
pi@opi-buster:~/quickjs$ qjs george/pi.js 
3.1415924535897797,2563ms
*/
