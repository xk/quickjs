#!/usr/bin/env qjs

/*
"use strip";
"use strict";

import * as std from "std";
import * as os from "os";

//Ejecutar esto así: ./qjs --std --script
*/

if (typeof setTimeout === 'undefined') {
	globalThis.setTimeout = function (f, t) {
		os.setTimeout(f, t);
	};
}

if (typeof setImmediate === 'undefined') {
	globalThis.setImmediate = function (f) {
		os.setTimeout(f, 0);
	};
}

var ctr= 0;

function cuenta () {
    ctr+= 1;
    setImmediate(cuenta);
}

function display () {
    console.log(ctr+'/s');
    ctr= 0;
    setTimeout(display, 1000);
}

setTimeout(display, 1000);
cuenta();

/*
pi@opi-buster:~/quickjs/george$ nodejs cuenta.js
97730/s
112972/s
116299/s
115913/s
^C
pi@opi-buster:~/quickjs/george$ qjs cuenta.js
243425/s
288820/s
288862/s
289013/s
^C
*/
