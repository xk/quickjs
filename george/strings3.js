
var str;
var r= [];
var max= 0;
var words= "Yes QuickJS starts faster than node because it is smaller and was optimized to have a low startup time. However, it is much slower for compute intensive tasks";
words= words.split(" ");

function rnd (n) {
	return Math.floor(Math.random()* n);
}

function makeStr (len) {
	//Append a word at a time.
	var s= "";
	while (s.length < len) s+= words[rnd(words.length)];
	return s;
}

for (var i=1 ; i<=512 ; i*= 2) {
	
	var t= Date.now();
	str= makeStr(1024*i);
	t= Date.now()- t;
	
	if (typeof std !== "undefined")
		std.out.printf('.'), std.out.flush();
	else if (typeof process !== "undefined")
		process.stdout.write('.');
		
	r.push({l:str.length, t:t});
	if (t>max) max= t;
}

console.log("\n");

r.forEach(function (v,i,o) {
	var len= Math.round(80* v.t/ max);
	str= "";
	while (str.length < len) str+= "*";
	console.log(str+ " "+ v.l+ " ("+ v.t+ " ms)");
});

/*
pi@opi-buster:~/quickjs/george$ nodejs strings3.js 
..........

* 1027 (1 ms)
** 2049 (2 ms)
** 4097 (2 ms)
**** 8193 (4 ms)
***************************** 16390 (31 ms)
******************************** 32769 (34 ms)
**** 65539 (4 ms)
********* 131079 (10 ms)
******************************************************************************** 262146 (86 ms)
******************************************************************************** 524290 (86 ms)
pi@opi-buster:~/quickjs/george$ qjs strings3.js 
..........

 1024 (2 ms)
 2049 (4 ms)
 4096 (8 ms)
 8197 (23 ms)
 16386 (62 ms)
 32769 (88 ms)
* 65541 (358 ms)
** 131076 (1243 ms)
********************************* 262144 (17243 ms)
******************************************************************************** 524289 (41550 ms)
*/
