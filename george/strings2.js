
var str= "";
var words= "Yes QuickJS starts faster than node because it is smaller and was optimized to have a low startup time. However, it is much slower for compute intensive tasks";
words= words.split(" ");

function rnd (n) {
	return Math.floor(Math.random()* n);
}

var t= Date.now();

//Append a word at a time.
while (str.length < 128*1024) str+= words[ rnd(words.length) ];

t= Date.now()- t;
console.log([t+ "ms", str.length]);

/*
pi@opi-buster:~/quickjs/george$ qjs strings2.js 
2490ms,131073
pi@opi-buster:~/quickjs/george$ nodejs strings2.js 
[ '79ms', 131077 ]
*/
